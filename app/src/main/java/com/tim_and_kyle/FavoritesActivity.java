package com.tim_and_kyle;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;

import java.util.List;

public class FavoritesActivity extends MenuActivity {
    private RecyclerView recyclerView;
    private RestoAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private List<RestoObject> restos;
    private RestoController restoController;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);

        this.restoController = new RestoController(this);

        restos = restoController.getAllFavorites();

        this.recyclerView = (RecyclerView) findViewById(R.id.RecyclerViewFav);

        this.layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        this.adapter = new RestoAdapter(restos);

        RecyclerView.LayoutManager lm = new GridLayoutManager(this, 2);

        this.recyclerView.setLayoutManager(lm);

        this.recyclerView.setAdapter(adapter);

    }
    /**
     * Creates a notification when user leaves the app
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("RETARD", "RETARD ITS DELETING");
        createNotificationChannel();
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "1").setSmallIcon(R.drawable.icons_trashcan).setContentTitle("Notification").setContentText("Don't forget your trash").setPriority(NotificationCompat.PRIORITY_DEFAULT);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(0, builder.build());


    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Notification for favorite";
            String description = "jsut a test channel";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("1", name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    /**
     *
     * @param item
     * @return the item that gets selected
     */
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch(item.getItemId()) {

            case 1:
                adapter.removeItem(item.getGroupId());
                return true;

            case 2:
                return true;

            default:
                return super.onContextItemSelected(item);
        }
    }
}

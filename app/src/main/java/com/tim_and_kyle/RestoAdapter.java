package com.tim_and_kyle;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.BundleCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class RestoAdapter extends RecyclerView.Adapter<RestoViewHolder>{
    private List<RestoObject> restos;
    private Context context;
    public RestoAdapter() {

    }

    /**
     * @param restos
     */
    public RestoAdapter(List<RestoObject> restos) {
        this.restos = restos;
    }

    /**
     *
     * @param parent
     * @param viewType
     * @return
     */
    public RestoViewHolder onCreateViewHolder(final ViewGroup parent, int viewType)
    {
        final Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.layout_item, parent, false);
        RestoViewHolder viewHolder = new RestoViewHolder(view, this, restos);

        CardView card = view.findViewById(R.id.card_view);


        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TextView hiddenReview = v.findViewById(R.id.hiddenReview);

                RestoFragment fragment = new RestoFragment();

                TextView txtname = v.findViewById(R.id.nameCard);

                TextView txtdesc = v.findViewById(R.id.hiddenDesc);

                ImageView image = v.findViewById(R.id.image);

                Bitmap bmap = ((BitmapDrawable) image.getDrawable()).getBitmap();
                ByteArrayOutputStream bastream = new ByteArrayOutputStream();
                bmap.compress(Bitmap.CompressFormat.JPEG, 100, bastream);
                byte[] bytes = bastream.toByteArray();

                Bundle args = new Bundle();
                Log.i("desc", txtdesc.getText().toString());
                args.putString("name", txtname.getText().toString());
                args.putString("desc", txtdesc.getText().toString());
                args.putByteArray("bytes", bytes);
                args.putString("review", hiddenReview.getText().toString());
                fragment.setArguments(args);
                FragmentManager ft;
                try {
                    ft = ((MainActivity) context).getSupportFragmentManager();
                    fragment.show(ft, "frag");
                } catch (ClassCastException e) {
                    ft = ((FavoritesActivity) context).getSupportFragmentManager();
                    fragment.show(ft, "frag");
                }
                }


        });
        return viewHolder;
    }


    /**
     *
     * @param holder
     * @param position
     */
    public void onBindViewHolder(RestoViewHolder holder, int position) {
        RestoObject restoObject = restos.get(position);
        holder.setResto(restoObject);
    }


    /**
     *
     * @return size of the list
     */
    @Override
    public int getItemCount() {
        return restos.size();
    }

    /**
     * Ability to remove restaurants from the list that gets populated in the main activity
     * @param position gets the position of the restaurant that you are selecting
     */
    public void removeItem(int position){
        RestoObject resto = new RestoObject();
        resto.setName(restos.get(position).getName());
        resto.setDescription(restos.get(position).getDescription());
        resto.setImage(restos.get(position).getImage());
        RestoController restoController = new RestoController(context);
        restoController.removeFavorite(resto);
        restos.remove(position);
        notifyDataSetChanged();
    }

    /**
     * Ability to add restaurants to the favorites activity
     * @param position gets the position of the restaurant that you are selecting
     */
    public void addItem(int position) {
        RestoObject resto = new RestoObject();
        resto.setName(restos.get(position).getName());
        resto.setDescription(restos.get(position).getDescription());
        resto.setImage(restos.get(position).getImage());
        RestoController restoController = new RestoController(context);
        restoController.createFavorite(resto);
        //notifyItemInserted(position);
    }

    /**
     * Used to update the list when you search for a restaurant
     * @param newList takes in the list of restaurants
     */
    public void updateSearchList(List<RestoObject> newList){
        restos = new ArrayList<>();
        restos.addAll(newList);
        notifyDataSetChanged();
    }
}


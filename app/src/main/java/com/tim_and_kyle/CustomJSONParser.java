package com.tim_and_kyle;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.tim_and_kyle.RestoObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class CustomJSONParser {

    public List<RestoObject> parse(String result) {
        final String RATING = "aggregate_rating";
        final String NAME = "name";
        final String URL = "photos_url";

        List<RestoObject> restoObjectList = new ArrayList();

        try {
              JSONObject jresponse = new JSONObject(result);
              JSONArray restos = jresponse.getJSONArray("restaurants");

              for (int i = 0; i < restos.length(); i++) {

              JSONObject restaurant = restos.getJSONObject(i);

              JSONObject generalInfo = restaurant.getJSONObject("restaurant");

              JSONObject userRating = generalInfo.getJSONObject("user_rating");


              RestoObject restoObject = new RestoObject();

              restoObject.setName(generalInfo.getString("name"));

                Log.i("RETARD", "Name: " + restoObject.getName());
            }
        } catch (JSONException e) {
            Log.i("TAG", e.getMessage(), e);
        }
        return restoObjectList;
}}

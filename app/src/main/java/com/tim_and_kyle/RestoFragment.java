package com.tim_and_kyle;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

public class RestoFragment extends DialogFragment implements View.OnClickListener{
    private RestoController restoController;
    private RestoAdapter restoAdapter;
    private ViewGroup viewGroup;
    private RestoObject oldRestoObject = new RestoObject();
    private RestoObject newRestoObject = new RestoObject();
    private EditText editText;
    private String name;
    private String description;
    private int rating;
    private ImageView image;
    private String review;
    RatingBar ratingBar;
    byte[] bytes;
    Bitmap bmp;
    private Context context = getContext();
    Refresh mCallBack;
    public MediaPlayer mp3;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mp3 = MediaPlayer.create(getContext(), R.raw.door);
        mp3.start();

        System.out.println("Creating Fragment...");
        View v = inflater.inflate(R.layout.fragment_resto, container, false);
        Bundle args = getArguments();
        name = args.getString("name");
        description = args.getString("desc");
        bytes = getArguments().getByteArray("bytes");
        bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

        image = v.findViewById(R.id.image);

        Button btn = v.findViewById(R.id.favoritebutton);

        TextView nameTxt = v.findViewById(R.id.name);
        TextView descTxt = v.findViewById(R.id.description);
        TextView revTxt = v.findViewById(R.id.review);
        ratingBar = v.findViewById(R.id.ratingBar);

        descTxt.setText(description);

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                restoController = new RestoController(getContext());
                oldRestoObject.setName(name);
                newRestoObject.setRating(Math.round(ratingBar.getRating()));
                restoController.updateRestos(oldRestoObject, newRestoObject);
                mp3 = MediaPlayer.create(getContext(), R.raw.trashcan);
                mp3.start();

            }
        });

        editText = v.findViewById(R.id.edit);
        //editText.setHint(args.getString("review"));
        revTxt.setText(args.getString("review"));

        nameTxt.setText(name);
        descTxt.setText(description);
        //descTxt.setText("retard");
        image.setImageBitmap(bmp);

        btn.setOnClickListener(this);
        MainActivity mainActivity = new MainActivity();
        restoAdapter = mainActivity.getAdapter();
        //viewGroup = mainActivity.getViewGroup();
        return v;
    }

    /**
     *
     * @param activity
     */
    @Override
   public void onAttach(Activity activity) {
              super.onAttach(activity);
                       try{
                      mCallBack = (Refresh) activity;
                    } catch(ClassCastException e){
                      throw new ClassCastException(activity.toString() + "must implement Refresh");
                    }
          }

    /**
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        System.out.println("Updating Button Clicked...");
        restoController = new RestoController(getContext());

        String edittxt = editText.getText().toString();
        oldRestoObject.setName(name);
        Log.i("TAG", Math.round(ratingBar.getRating()) + "");
        oldRestoObject.setReview(edittxt);

        newRestoObject.setReview(edittxt);
        restoController.updateRestos(oldRestoObject, newRestoObject);
        mCallBack.refresh();

    }


}

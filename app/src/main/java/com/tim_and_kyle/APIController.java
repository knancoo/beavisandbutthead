package com.tim_and_kyle;

import android.os.NetworkOnMainThreadException;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.PortUnreachableException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class APIController {
    private final String TAG = "APIController";
    public APIController() {

    }

    /**
     * parses the url that is being passed to the method with a user-key
     * @param endpoint is the url that is being passed
     * @return the response of the API
     */
    public String makeAPIController(String endpoint) {
        String response = "";

        try {
            URL url = new URL(endpoint);
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setRequestProperty("user-key", "e77ec7bff43511ec796b8d0794840a0e");
            conn.connect();

            InputStream in = conn.getInputStream();
            StringBuilder stringBuilder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String line;

            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }

            response = stringBuilder.toString();
        } catch (MalformedURLException me) {
            Log.e(TAG, me.getMessage(), me);
        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
        } catch (NetworkOnMainThreadException t) {
            Log.e(TAG, t.getMessage(), t);
        }
        return response;
    }
}

package com.tim_and_kyle;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBAccessController {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DBAccessController instance;

    /**
     *
     * @param context gets the context our database
     */
    private DBAccessController(Context context) {
        this.openHelper = new RestoDatabaseOpenHelper(context);
    }

    /**
     *
     * @param context gets the context our database
     * @return the instance of the database
     */
    public static synchronized DBAccessController getInstance(Context context) {
        if (instance == null) {
            instance = new DBAccessController(context);
        }
        return instance;
    }

    /**
     * method used to open the database whenever you need to modify it
     * @return the database after opening
     */
    public SQLiteDatabase openDatabase(){
        this.database = openHelper.getWritableDatabase();
        return this.database;
    }

    /**
     * method used to close the database whenever you are finished modifying
     */
    public void closeDatabase() {
        if (database != null) {
            this.database.close();
        }
    }
}

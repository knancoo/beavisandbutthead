package com.tim_and_kyle;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class RestoViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {
    RestoAdapter restoAdapter;
    private TextView txtName;
    private TextView txtDescription;
    private ImageView image;
    private EditText editText;
    private TextView hiddenReview;
    private TextView hiddenDesc;
    private  List<RestoObject> restos;
    private RestoObject resto;
    CardView cardView;

    public RestoViewHolder(View firstItemRowView, RestoAdapter iRestoAdapter, List<RestoObject> iRestos) {
        super(firstItemRowView);


        this.restos = iRestos;
        this.restoAdapter = iRestoAdapter;
        this.txtName = (TextView) firstItemRowView.findViewById(R.id.nameCard);
        this.hiddenReview = (TextView) firstItemRowView.findViewById(R.id.hiddenReview);
        this.image = (ImageView) firstItemRowView.findViewById(R.id.image);
        this.hiddenDesc = (TextView) firstItemRowView.findViewById(R.id.hiddenDesc);
        cardView = itemView.findViewById(R.id.card_view);
        cardView.setOnCreateContextMenuListener(this);


    }

    /**
     *
     * @param resto object that sets all the information
     */

    public void setResto(RestoObject resto) {
        this.resto = resto;
        this.txtName.setText(resto.getName());
        this.image.setImageBitmap(resto.getImage());
        this.hiddenReview.setText(resto.getReview());
        this.hiddenDesc.setText(resto.getDescription());
        RestoFragment fragment = new RestoFragment();
        //fragment;
    }

    /**
     * @param menu
     * @param v
     * @param menuInfo
     */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.add(this.getAdapterPosition(), 1, 0, "Delete");
        menu.add(this.getAdapterPosition(), 2, 0, "Add to Favorites");
    }

}

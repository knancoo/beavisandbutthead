package com.tim_and_kyle;

import android.graphics.Bitmap;

public class RestoObject {
    private int id;
    private String name;
    private String description = "LOL RETARD";
    private Bitmap image;
    private int rating;
    private String review;
    private double latitude;
    private double longitude;

    /**
     * Constructor that takes in the name of the resto and description
     * @param name of the resto
     * @param description of the resto
     */
    public RestoObject(String name, String description) {
        this.name = name;
        this.description = description;
    }

    /**
     * Required empty constructor
     */
    public RestoObject() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() { return description; }

    public void setDescription(String description) {
        this.description = description;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}

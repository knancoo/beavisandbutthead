package com.tim_and_kyle;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SplashScreen extends AppCompatActivity {
    private RestoController restoController;
    private int calls;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        restoController = new RestoController(this);

        SharedPreferences sharedPreferences = getSharedPreferences("calls", MODE_PRIVATE);

        calls = sharedPreferences.getInt("calls", 0);

        calls = calls + 1;

        sharedPreferences.edit().putInt("calls", calls).commit();

        Thread myT = new Thread(){
            @Override
            public void run() {
                try {
                    loadData();
                    sleep(8000);
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    finish();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        myT.start();

        Thread myT2 = new Thread(){
            @Override
            public void run() {
                findViewById(R.id.loadingPanel).setVisibility(View.GONE);
            }
        };

    }

    private void loadData() {
        final List<RestoObject> listRestoObject = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                "https://developers.zomato.com/api/v2.1/search?lat=45.50884&lon=-73.58781",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jresponse = new JSONObject(response);
                            JSONArray restos = jresponse.getJSONArray("restaurants");

                            for (int i = 0; i < restos.length(); i++) {

                                Log.i("RESTOS", restos.length() + "");

                                JSONObject restaurant = restos.getJSONObject(i);

                                JSONObject generalInfo = restaurant.getJSONObject("restaurant");

                                JSONObject userRating = generalInfo.getJSONObject("user_rating");


                                RestoObject restoObject = new RestoObject();

                                restoObject.setName(generalInfo.getString("name"));

                                //restoObject.setDescription(generalInfo.getString("cuisine"));

                                /*try {
                                    URL url = new URL(generalInfo.getString("featured_image"));
                                    image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                                    restoObject.setImage(image);
                                } catch (IOException e) {
                                    System.out.print(e);
                                }*/

                                Drawable draw = (getResources().getDrawable(R.drawable.abandonedpizzahut));
                                Bitmap bmp      = ((BitmapDrawable) draw).getBitmap();
                                restoObject.setImage(bmp);

                                listRestoObject.add(restoObject);
                                restoController.createResto(restoObject);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }



                }){

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user-key", "e77ec7bff43511ec796b8d0794840a0e");
                return params;
            }
        };



        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);


    }
}

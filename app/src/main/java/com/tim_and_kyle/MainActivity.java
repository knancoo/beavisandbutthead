package com.tim_and_kyle;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends MenuActivity implements SearchView.OnQueryTextListener, Refresh {

    private ConstraintLayout cL;
    private RecyclerView recyclerView;
    private RestoAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private List<RestoObject> restos;
    private RestoController restoController;
    String response = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.restoController = new RestoController(this);
        restos = restoController.getAllRestos();

        Log.i("RETARD", "SIZE:");

        this.recyclerView = (RecyclerView) findViewById(R.id.RecyclerView);

        this.layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        this.adapter = new RestoAdapter(restos);

        RecyclerView.LayoutManager lm = new GridLayoutManager(this, 2);

        this.recyclerView.setLayoutManager(lm);

        this.recyclerView.setAdapter(adapter);


        BottomNavigationView bottomNav = (BottomNavigationView) findViewById(R.id.menu_nav);
        bottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {

                    case R.id.Favorites: {
                        Intent intent = new Intent(getApplicationContext(), FavoritesActivity.class);
                        startActivity(intent);
                        break;
                    }

                    case R.id.Home: {
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    }
            }
                return true;
            }

        });
    }

    /**
     * Creates a notification when user leaves the app
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("RETARD", "RETARD ITS DELETING");
        createNotificationChannel();
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "1").setSmallIcon(R.drawable.icons_trashcan).setContentTitle("Notification").setContentText("Don't forget your trash").setPriority(NotificationCompat.PRIORITY_DEFAULT);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(0, builder.build());


    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Notification for favorite";
            String description = "jsut a test channel";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("1", name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }



    /**
     *
     * @param item is a specific item in the context menu
     * @return items in the context menu with its respective actions
     */
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch(item.getItemId()) {

            case 1:
                adapter.removeItem(item.getGroupId());
                return true;

            case 2:
                adapter.addItem(item.getGroupId());

                Snackbar sB = Snackbar.make(findViewById(R.id.coordinatorL), "Added to Favorites", Snackbar.LENGTH_LONG);
                sB.setAction("Go to Favorites", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), FavoritesActivity.class);
                        finish();
                        startActivity(intent);
                    }
                });
                sB.show();
                return true;

            default:
                return super.onContextItemSelected(item);
        }
    }

    /**
     *
     * @param menu takes in the menu that is being created
     * @return true to show the menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        MenuItem menuItem = menu.findItem(R.id.search_action);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(this);


        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    /**
     *
     * @param newText takes in search input from user
     * @return the list based on the search the user has entered
     */
    @Override
    public boolean onQueryTextChange(String newText) {

        String input = newText.toLowerCase();
        List<RestoObject> newList = new ArrayList<>();

        for(RestoObject object: restos){
            if(object.getName().toLowerCase().contains(input)){
                newList.add(object);
            }
        }

        adapter.updateSearchList(newList);
        return true;
    }

    /**
     *
     * @return reference of the adapter
     */
    public RestoAdapter getAdapter() {
        return this.adapter;
    }

    /**
     * method to refresh the main activity
     */
    public void refresh(){

        this.adapter.notifyDataSetChanged();
        Intent intent = new Intent(this, MainActivity.class);
        finish();
        startActivity(intent);
    }
}

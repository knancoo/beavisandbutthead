package com.tim_and_kyle;

import android.content.Context;

import com.tim_and_kyle.helper.SQLiteAssetHelper;
import com.tim_and_kyle.helper.SQLiteAssetHelper;

public class RestoDatabaseOpenHelper extends SQLiteAssetHelper {
    private static final String DATABASE_NAME = "restodb.db";
    private static final int DATABASE_VERSION = 1;

    public RestoDatabaseOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
}

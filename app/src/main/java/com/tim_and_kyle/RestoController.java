package com.tim_and_kyle;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.ContactsContract;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class RestoController {
    private DBAccessController databaseAccessHelper;
    private SQLiteDatabase sqLiteDatabase;
    private static final String TAG = RestoController.class.getSimpleName();

    /**
     *
     * @param context
     */
    public RestoController(Context context) {
        this.databaseAccessHelper = DBAccessController.getInstance(context);
    }

    /**
     * Retrieves all the restaurants from the database
     * @return the list of the restaurants
     */
    public List<RestoObject> getAllRestos() {
        List<RestoObject> restoList = new ArrayList<>();
        sqLiteDatabase = this.databaseAccessHelper.openDatabase();
        try {
            Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM Restos", null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                RestoObject resto = new RestoObject();

                resto.setId(cursor.getInt(cursor.getColumnIndex("id")));

                resto.setName(cursor.getString(cursor.getColumnIndex("name")));

                Log.i("DB", "GET: " + cursor.getColumnIndex("description"));

                resto.setDescription(cursor.getString(cursor.getColumnIndex("description")));

                resto.setRating(cursor.getInt(cursor.getColumnIndex("rating")));

                resto.setReview(cursor.getString(cursor.getColumnIndex("review")));

                byte[] bytearray = cursor.getBlob(cursor.getColumnIndex("image"));

                Bitmap bmp = BitmapFactory.decodeByteArray(bytearray, 0, bytearray.length);

                resto.setImage(bmp);

                // System.out.println(bytearray.toString());

                restoList.add(resto);
                cursor.moveToNext();
            }
            cursor.close();
        } catch (Exception e) {
            Log.i("DB", e + "");
            e.printStackTrace();
        } finally {
            Log.i("DB", "FINALLY");
            this.databaseAccessHelper.closeDatabase();
        }
        return restoList;
    }

    /**
     * Retrieves all the restaurants that is currently in the favorites table in the database
     * @return the list of restaurants that are in the favorites activity
     */
    public List<RestoObject> getAllFavorites() {
        List<RestoObject> restoList = restoList = new ArrayList<>();
        sqLiteDatabase = this.databaseAccessHelper.openDatabase();
        try {
            Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM Favorites", null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {

                RestoObject resto = new RestoObject();

                resto.setId(cursor.getInt(cursor.getColumnIndex("id")));

                resto.setName(cursor.getString(cursor.getColumnIndex("name")));

                resto.setDescription(cursor.getString(cursor.getColumnIndex("description")));

                resto.setRating(cursor.getInt(cursor.getColumnIndex("rating")));

                resto.setReview(cursor.getString(cursor.getColumnIndex("review")));

                byte[] bytearray = cursor.getBlob(cursor.getColumnIndex("image"));

                Bitmap bmp = BitmapFactory.decodeByteArray(bytearray, 0, bytearray.length);

                resto.setImage(bmp);
                restoList.add(resto);
                cursor.moveToNext();
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            this.databaseAccessHelper.closeDatabase();
        }
        Log.i("tag", restoList.size() + "");
        return restoList;
    }

    /**
     * Creates the favorite restaurant that the user favorited in the favorite activity and inserts
     * it into the database
     * @param newResto takes in the object
     */
    public void createFavorite(RestoObject newResto) {
        sqLiteDatabase = this.databaseAccessHelper.openDatabase();
        sqLiteDatabase.beginTransaction();
        try {
            ContentValues values = new ContentValues();

            values.put("name", newResto.getName());

            values.put("description", newResto.getDescription());

            values.put("rating", newResto.getRating());

            values.put("review", newResto.getReview());

            Bitmap img = newResto.getImage();

            ByteArrayOutputStream stream = new ByteArrayOutputStream();

            img.compress(Bitmap.CompressFormat.PNG, 100, stream);

            byte[] bytearray = stream.toByteArray();

            values.put("image", bytearray);

            sqLiteDatabase.insert("Favorites", null, values);

            sqLiteDatabase.setTransactionSuccessful();
        } catch (Exception e) {
            Log.wtf(TAG, e.fillInStackTrace());
        } finally {
            sqLiteDatabase.endTransaction();
            this.databaseAccessHelper.closeDatabase();
        }
    }

    /**
     * Creates the restaurants, sets the data, and puts information into the database
     * @param newResto takes in the object
     */
    public void createResto(RestoObject newResto) {
        sqLiteDatabase = this.databaseAccessHelper.openDatabase();
        sqLiteDatabase.beginTransaction();
        try {
            ContentValues values = new ContentValues();

            values.put("name", newResto.getName());

            Log.i("DB", "PUT: " + newResto.getName());

            values.put("description", newResto.getDescription());

            values.put("rating", newResto.getRating());

            values.put("review", newResto.getReview());

            Bitmap img = newResto.getImage();

            ByteArrayOutputStream stream = new ByteArrayOutputStream();

            img.compress(Bitmap.CompressFormat.PNG, 100, stream);

            byte[] bytearray = stream.toByteArray();

            values.put("image", bytearray);


            sqLiteDatabase.insert("Restos", null, values);

            sqLiteDatabase.setTransactionSuccessful();
        } catch (Exception e) {
            Log.wtf(TAG, e.fillInStackTrace());
        } finally {
            sqLiteDatabase.endTransaction();
            this.databaseAccessHelper.closeDatabase();
        }
    }

    /**
     * Called when the option "delete" is selected in the context menu in the favorties activity
     * @param newResto takes in the object
     */
    public void removeFavorite(RestoObject newResto) {
        sqLiteDatabase = this.databaseAccessHelper.openDatabase();
        try {
            sqLiteDatabase.delete("Favorites", "name = ?", new String[]{
                    newResto.getName()});
            } catch (Exception e) {
            Log.wtf(TAG, e.fillInStackTrace());
        } finally {
            sqLiteDatabase.close();
            this.databaseAccessHelper.closeDatabase();
        }
    }

    /**
     * Used to update the reviews on the fragment when a user submits a new review
     * @param oldResto old resto object that contains the old review
     * @param newResto new resto object that contains new review
     */
    public void updateRestos(RestoObject oldResto, RestoObject newResto) {
        sqLiteDatabase = this.databaseAccessHelper.openDatabase();
        sqLiteDatabase.beginTransaction();

        System.out.println("Updating Restaurant...");

        try {
            ContentValues values = new ContentValues();
            values.put("review", newResto.getReview());
            values.put("rating", newResto.getRating());

            sqLiteDatabase.update("Restos", values, "name = '" + oldResto.getName() + "'", null);
            sqLiteDatabase.setTransactionSuccessful();
        } catch (Exception e) {
            Log.wtf(TAG, e.fillInStackTrace());
        } finally {
            sqLiteDatabase.endTransaction();
            this.databaseAccessHelper.closeDatabase();
        }
    }
}



# BeavisAndButtHead - Disastrous Dining App

## Description of App: ##

* Our application is similar to other restaurant apps out there. The difference is based off the goal of app, 
which is to display the worst rated restaurants nearby to the morbidly curious restaurant diner.


## List of Features Proposed: ##

*   Find the worst rated restaurants around
*	Gain points for dining at said restaurants
*	Points feature to allow fellow Disastrous Dining users to show off badges and avatars to friends and other users of the app.
*	Set a timer from the start of your journey to the restaurant, where you will
    Receive points if you make it in time.
*	Make reviews directly on the app after your experience. This will be done using the zomato API.
    Implementation of the worst restaurants updated depending on which restaurants are currently the worst on that day.
*	Ability to mark a restaurant that you had an awful experience with, so you don’t go to that restaurant again.


## List of Features that were implemented ##

*	Find the worst rated restaurants around
*	Make reviews directly on the app after your experience. This will be done using the zomato API.
    Implementation of the worst restaurants updated depending on which restaurants are currently the worst on that day.
*	Favorited section to mark the worst of the worst restaurants.
*	Funny notification to remind you about all those trash restaurants you could be visiting right now.
*	Search Ability in order to filter restaurants by name.


### API Used in our app: ###
* [Zomato API](https://developers.zomato.com/documentation) <-- All documentation for the API